

var schedule_obj =
[
    {
        "display_time": "7-9 a.m.",
        "end": "1413205200000",
        "speakers": [
            {
                "name": "Sgt. Maj. of the Army Raymond F. Chandler III"
            }
        ],
        "start": "1413198000000",
        "title": "Sergeant Major of the Army, Noncommissioned Officer and Soldier Forum",
        "twitter": "2014",
        "video_id": "4948"
    },
    {
        "display_time": "9:30 - 11:15 a.m.",
        "end": "1413213300000",
        "keynote_speaker": [
            {
                "name": "Secretary of the Army John M. McHugh",
                "type": "keynote speaker"
            }
        ],
        "start": "1413207000000",
        "title": "Opening Ceremony",
        "twitter": "2014",
        "video_id": "4950"
    },
    {
        "display_time": "11:45 a.m. \u2013 12:15 p.m.",
        "end": "1413217800000",
        "speakers": [
            {
                "name": "Secretary of the Army John M. McHugh"
            },
            {
                "name": "Chief of Staff of the Army Gen. Raymond T. Odierno"
            }
        ],
        "start": "1413215100000",
        "title": "Press Conference",
        "twitter": "2014",
        "video_id": "4952"
    },
    {
       
        "display_time": "1-4 p.m.",
        "end": "1413225000000",
        "moderator": [
            {
                "name": "Carla K. Coulson",
                "rank": "Director, Installation Services",
                "type": "moderator"
            }
        ],
        "panelists": [
            {
                "name": "Lt. Gen. David D. Halverson",
                "rank": "Assistant Chief of Staff/Commanding General, Installation Management Command",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen. Jeffrey W. Talley",
                "rank": "Chief of Army Reserve/Commanding General, U.S. Army Reserve Command",
                "type": "panelists"
            },
            {
                "name": "Maj. Gen. Judd H. Lyons",
                "rank": "Acting Director, Army National Guard",
                "type": "panelists"
            }
        ],
        "closing_remarks": [
            {
                "name": "Lt. Gen. Patricia D. Horoho",
                "rank": "Surgeon General, Commanding General, U.S. Army Medical Command",
                "type": "closing remarks"
            }
        ],
        "start": "1413219600000",
        "subtitle": "Senior Leader's Update and Town Hall",
        "title": "AUSA Military Family Forum I",
        "twitter": "FAM2014",
        "video_id": "4955"
    },
    {
        "display_time": "3-5 p.m.",
        "end": "1413234000000",
        "introductions_by": [
            {
                "name": "Retired Gen. Gordon R. Sullivan",
                "rank": "AUSA President",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Lt. Gen.  Robert B. Brown",
                "rank": "Commanding General, U.S. Army Combined Arms Center",
                "type": "lead speaker"
            }
        ],
        "panelists": [
            {
                "name": "Gen. Ramond T. Odierno",
                "rank": "Chief of Staff of the Army",
                "type": "panelists"
            },
            {
                "name": "Gen. David G. Perkins",
                "rank": "Commanding General, U.S. Army Training and Doctrine Command",
                "type": "panelists"
            },
            {
                "name": "Sgt. Maj. Raymond F. Chandler III",
                "rank": "Sergeant Major of the Army",
                "type": "panelists"
            },
            {
                "name": "Karl F. Schneider",
                "rank": "Principal Deputy Assistant Secretary of the Army (Manpower and Reserve Affairs)",
                "type": "panelists"
            },
            {
                "name": "Dr. Don Snider",
                "rank": "Ph.D., CAPE Senior Fellow",
                "type": "panelists"
            }
        ],
        "start": "1413226800000",
        "subtitle": "Living the Army Ethic:\"Why and How We Serve\"",
        "title": "ILW Contemporary Military Forum I",
        "twitter": "CMF",
        "video_id": "4958"
    },
    {
        "display_time": "3-5 p.m.",
        "end": "1413234000000",
        "introductions_by": [
            {
                "name": "Retired Maj. Gen. Raymond W. Carpenter",
                "rank": "U.S. Army, AUSA Senior Fellow",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Gen. Mark A. Milley",
                "rank": "Commanding General, U.S. Army Forces Command",
                "type": "lead speaker"
            }
        ],
        "panelists": [
            {
                "name": "Paul D. Patrick",
                "rank": "Deputy Assistant Secretary, Defense Reserve Affairs (Readiness, Training and Mobilization)",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen. Jeffrey W. Talley",
                "rank": "Chief of Army Reserve/Commanding General, U.S. Army Reserve Command",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen. Michael S. Tucker",
                "rank": "Commanding General, First U.S. Army",
                "type": "panelists"
            },
            {
                "name": "Maj. Gen. Judd H. Lyons",
                "rank": "Acting Director, Army National Guard",
                "type": "panelists"
            }
        ],
        "start": "1413226800000",
        "subtitle": "Army Total Force Implementation",
        "title": "ILW Contemporary Military Forum II",
        "twitter": "CMF",
        "video_id": "4959"
    },
    {
        "display_time": "8:30-11 a.m.",
        "end": "1413298800000",
        "speakers": [
            {
                "name": "Sgt. Maj. David Stewart",
                "rank": "senior enlisted adviser, Center for the Army Profession and Ethic"
            }
        ],
        "start": "1413289800000",
        "subtitle": "The Army Profession",
        "title": "The Sergeant Major of the Army\u2019s Professional Development Forum",
        "twitter": "2014",
        "video_id": "4962"
    },
    {
        "display_time": "10 a.m. to noon",
        "end": "1413302400000",
        "introductions_by": [
            {
                "name": "Retired Gen. Leon Salomon",
                "rank": "U.S. Army, AUSA Senior Fellow",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Katherine Hammack",
                "rank": "Assistant Secretary of the Army (Installations, Energy and Environment)",
                "type": "lead speaker"
            }
        ],
        "opening_remarks": [
            {
                "name": "Sgt. Maj. of the Army Raymond F. Chandler III",
                "type": "opening remarks"
            }
        ],
        "panelists": [
            {
                "name": "Lt. Gen. Thomas P. Bostick",
                "rank": "Chief of Engineers/Commanding General, U.S. Army Corps of Engineers",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen. David D. Halverson",
                "rank": "Assistant Chief of Staff/Commanding General, Installation Management Command",
                "type": "panelists"
            },
            {
                "name": "Steve Chuslo",
                "rank": "Executive Vice President/General Council, Hannon and Armstrong",
                "type": "panelists"
            },
            {
                "name": "Timothy Unruh",
                "rank": "Director, Federal Energy Management Program",
                "type": "panelists"
            },
            {
                "name": "Christopher C. Womack",
                "rank": "President of External Affairs, Executive Vice President of Southern Company",
                "type": "panelists"
            }
        ],
        "start": "1413295200000",
        "subtitle": "Resilient Installations: A Platform for Power Projection",
        "title": "ILW Contemporary Military Forum III",
        "twitter": "CMF",
        "video_id": "4963"
    },
    {
        "display_time": "10 a.m. to noon",
        "end": "1413302400000",
        "introductions_by": [
            {
                "name": "Retired Gen. Gordon R. Sullivan",
                "rank": "U.S. Army, AUSA President",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Lt. Gen. Perry L. Wiggins",
                "rank": "Commanding General, U.S. Army North/Fifth U.S. Army",
                "type": "lead speaker"
            }
        ],
        "moderator": [
            {
                "name": "Dr. David Applegate, Ph.D.",
                "rank": "Associate Director, Natural Hazards United States Geological Survey",
                "type": "moderator"
            }
        ],
        "panelists": [
            {
                "name": "Gen. Charles H. Jacoby, Jr.",
                "rank": "Commander, U,S. Northern Command/North American Aerospace Defense Command (via Skype)",
                "type": "panelists"
            },
            {
                "name": "Maj. Gen. John W. Peabody",
                "rank": "Deputy Commanding General, Civil and Emergency Operations, U.S. Army Corps of Engineers",
                "type": "panelists"
            },
            {
                "name": "Maj. Gen. Glenn J. Lesniak",
                "rank": "Deputy Commanding General for Support, U.S. Army Reserve Command",
                "type": "panelists"
            },
            {
                "name": "Brig. Gen. Gregory D. Mason",
                "rank": "Assistant Adjutant General-Maneuver, Missouri",
                "type": "panelists"
            },
            {
                "name": "Chief James Schwartz",
                "rank": "Fire Chief, Arlington County Department",
                "type": "panelists"
            },
            {
                "name": "Robert J. Fenton",
                "rank": "Assistant Administrator for Response, Federal Emergency Management Agency",
                "type": "panelists"
            }
        ],
        "special_address_by": [
            {
                "name": "Jeh C. Johnson",
                "rank": "Secretary of Homeland Security",
                "type": "special address by"
            }
        ],
        "start": "1413295200000",
        "subtitle": "Defense Support to Civil Authorities: Responding to America\u2019s Worst Day",
        "title": "ILW Contemporary Military Forum-Department of Homeland Security Seminar",
        "twitter": "CMF",
        "video_id": "4964"
    },
    {
		"description" : "Due to audio licensing regulations, the audio accompanying this video broadcast may drop out during certain points in playback. We are aware of this and do apologize for any inconvenience.",
        "display_time": "11:30 a.m. - 2:30 p.m.",
        "end": "1413303300000",
        "speaker": [
            {
                "name": "Gen. Raymond T. Odierno",
                "rank": "Chief of Staff U.S. Army"
            }
        ],
        "start": "1413300600000",
        "title": "Dwight David Eisenhower Luncheon",
        "twitter": "2014",
        "video_id": "5114"
    },
    {
        
        "display_time": "1 p.m. - 4 p.m.",
        "end": "1413316800000",
        "opening_speaker": [
            {
                "name": "Gary Sinise",
                "type": "opening speaker"
            }
        ],
        "moderator": [
            {
                "name": "Brig. Gen. Jason T. Evans",
                "rank": "Deputy Commanding General, Support, Installation Management Command",
                "type": "moderator"
            }
        ],
        "panelists": [
            {
                "name": "Retired Maj. Gen. Mark Graham",
                "rank": "U.S. Army, Director Vets4Warriors, Rutgers University Behavioral Health Care",
                "type": "panelists"
            },
            {
                "name": "Col. Adam L. Roche",
                "rank": "Special Assistant to the Chief of Staff of the Army/Director, Soldier for Life Program",
                "type": "panelists"
            },
            {
                "name": "Lynn McCollum",
                "rank": "Director, Family Programs, Installation Management Command G9, Family and MWR Programs",
                "type": "panelists"
            },
            {
                "name": "Christina Vine",
                "rank": "Office of the Assistant Chief of Staff, Installation Management Command, Soldier and Family Readiness Division",
                "type": "panelists"
            },
            {
                "name": "Noreen O\u2019Neill",
                "rank": "Military Spouse Program, Hiring Our Heroes, U.S. Chamber of Commerce Foundation",
                "type": "panelists"
            }
        ],
        "closing_remarks": [
            {
                "name": "Jacey Eckhart",
                "rank": "Director, Military.com Spouse and Family Programs",
                "type": "closing remarks"
            }
        ],
        "start": "1413306000000",
        "subtitle": "Family Transitions: Supporting You Through, Your Military Life Journey",
        "title": "AUSA Military Family Forum II",
        "twitter": "FAM2014",
        "video_id": "4970"
    },
    {
        "display_time": "3-5 p.m.",
        "end": "1413320400000",
        "introductions_by": [
            {
                "name": "Retired Lt. Gen. Terry Wolff",
                "rank": "U.S. Army, AUSA Senior Fellow",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Lt. Gen. Herbert R. McMaster, Jr.",
                "rank": "Deputy Commanding General, Futures/Director, Army Capabilities Integration Center, U.S. Army Training and Doctrine Command",
                "type": "lead speaker"
            }
        ],
        "panelists": [
            {
                "name": "Gen. David G. Perkins",
                "rank": "Commanding General, U.S. Army Training and Doctrine Command",
                "type": "panelists"
            },
            {
                "name": "Dr. Janine Davidson, Ph.D.",
                "rank": "Senior Fellow for Defense Policy, Council on Foreign Relations",
                "type": "panelists"
            },
            {
                "name": "Michael E. O'Hanlon, Ph.D.",
                "rank": "Senior Fellow Brookings Institution ",
                "type": "panelists"
            },
            {
                "name": "Linda Robinson",
                "rank": "Senior International Policy Analyst, Rand Corporation",
                "type": "panelists"
            }
        ],
        "start": "1413313200000",
        "subtitle": "Force 2025 and Beyond: Setting the Course",
        "title": "ILW Contemporary Military Forum IV",
        "twitter": "CMF",
        "video_id": "4967"
    },
    {
        "display_time": "3-5 p.m.",
        "end": "1413320400000",
        "introductions_by": [
            {
                "name": "Retired Gen. Gordon R. Sullivan",
                "rank": "U.S. Army, AUSA President",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Gen. Vincent K. Brooks",
                "rank": "Commanding General, U.S. Army Pacific",
                "type": "lead speaker"
            }
        ],
        "panelists": [
            {
                "name": "Gen. Kiyofumi Iwata",
                "rank": "Chief of Staff, Japan Ground Self-Defense Force",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen. Stephen R. Lanza",
                "rank": "Commanding General, I Corps, Joint Base Lewis-McChord",
                "type": "panelists"
            },
            {
                "name": "Maj. Gen.  James C. Boozer, Sr.",
                "rank": "Commanding General, U.S. Army Japan, I Corps (via VTC/Skype/DIVIDS)",
                "type": "panelists"
            },
            {
                "name": "Maj. Gen. Charles A. Flynn",
                "rank": "Commanding General, 25th Infantry Division",
                "type": "panelists"
            },
            {
                "name": "Col. Louis A. Zeisman",
                "rank": "Brigade Commander, 2nd Stryker Brigade Combat Team (via Skype)",
                "type": "panelists"
            },
            {
                "name": "Dr. Terrence Kelly, Ph.D.",
                "rank": "Director, Strategy and Resources Program, RAND Arroyo Center; Senior Operations Researcher; Professor, Pardee RAND Graduate School",
                "type": "panelists"
            },
            {
                "name": "Scott Marciel",
                "rank": "Principal Deputy Assistant Secretary, Bureau of East Asian and Pacific Affairs",
                "type": "panelists"
            }
        ],
        "start": "1413313200000",
        "subtitle": "Asia Pacific Rebalance \u2013 Pacific Pathways and Beyond",
        "title": "ILW Contemporary Military Forum V",
        "twitter": "CMF",
        "video_id": "4969"
    },
    {
        
        "display_time": "8:30 a.m. \u2013 12:30 p.m.",
        "end": "1413390600000",
        "part_i_moderator": [
            {
                "name": "Mary M. Keller, Ed.D",
                "rank": "President and CEO, Military Child Education Coalition",
                "type": "part i moderator"
            }
        ],
        "part_i_panelists": [
            {
                "name": "Col. Rebecca Porter",
                "rank": "Commander, U.S. Army Dunham Health Clinic",
                "type": "part i panelists"
            },
            {
                "name": "Col. Steve Cozza",
                "rank": "Professor of Psychiatry, Associate Director, Study of Traumatic Stress, Uniformed Services University",
                "type": "part i panelists"
            },
            {
                "name": "Dr. Shelley MacDermid Wadsworth, Ph.D",
                "rank": "Director, Center for Families/Director, Military Family Research Institute, Purdue University",
                "type": "part i panelists"
            },
            {
                "name": "Dr. Paula K. Raunch, MD",
                "rank": "Director, Marjorie E. Korff PACT Program/Program Director, Family Support and Outreach, Red Sox Foundation/MGH Home Base Program, Massachusetts General Hospital",
                "type": "part i panelists"
            }
        ],
        "part_ii_moderator": [
            {
                "name": "Karen Halverson",
                "rank": "part ii moderator"
            }
        ],
        "part_ii_panelists": [
            {
                "name": "Rene Bostick",
                "rank": "Principal, Randolph Elementary School, Arlington, Virginia",
                "type": "part ii panelists"
            },
            {
                "name": "Julie Broad",
                "rank": "Comprehensive Soldier and Family Fitness 2, Resilient Teen Pilot",
                "type": "part ii panelists"
            },
            {
                "name": "Dr. Mark Vagle, Ph.D",
                "rank": "Associate Professor, University of Minnesota",
                "type": "part ii panelists"
            },
            {
                "name": "Kathy Facon",
                "rank": "Chief, Educational Partnership and non-DoD School Program, DODEA",
                "type": "part ii panelists"
            }
        ],
        "speakers": [
            {
                "name": "John M. McHugh",
                "rank": "Secretary of the Army"
            },
            {
                "name": "Gen. Raymond T. Odierno",
                "rank": "Chief of Staff U.S. Army"
            },
            {
                "name": "Sgt. Maj. Raymond F. Chandler III",
                "rank": "Sergeant Major of the Army"
            }
        ],
        "closing_speaker": [
            {
                "name": "Linda Odierno",
                "rank": "",
                "type": "closing speaker"
            }
        ],
        "start": "1413376200000",
        "subtitle": "Senior Leaders\u2019 Town Hall/ The Future of Our of Our Military Children",
        "title": "AUSA Military Family Forum III",
        "twitter": "FAM2014",
        "video_id": "4980"
    },
    {
        "display_time": "9:30-11:30 a.m.",
        "end": "1413387000000",
        "introductions_by": [
            {
                "name": "Retired Lt. Gen. Janes M. Dubik",
                "rank": "U.S. Army, AUSA Senior Fellow",
                "type": "introductions by"
            }
        ],
        "lead speaker": [
            {
                "name": "Lt. Gen. Charles T. Cleveland",
                "rank": "Commanding General, U.S. Army Special Operations Command",
                "type": "lead speaker"
            }
        ],
        "panelists": [
            {
                "name": "Max Boot",
                "rank": "Senior Fellow Council on Foreign Relations",
                "type": "panelists"
            },
            {
                "name": "Sarah Sewell",
                "rank": "Under Secretary for Civilian Security, Democracy, and Human Rights U.S. State Department",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen.  Herbert R. McMaster, Jr.",
                "rank": "Deputy Commanding General, Futures/Director, Army Capabilities Integration Center, U.S. Army Training and Doctrine Command",
                "type": "panelists"
            },
            {
                "name": "Dr. Viva Bartkus, Ph.D.",
                "rank": "Professor, University of Notre Dame",
                "type": "panelists"
            }
        ],
        "start": "1413379800000",
        "subtitle": "Persistent Influence and the Strategic Quality of Landpower",
        "title": "ILW Contemporary Military Forum VI",
        "twitter": "CMF",
        "video_id": "4972"
    },
    {
        "display_time": "9:30-11:30 a.m.",
        "end": "1413387000000",
        "introductions_by": [
            {
                "name": "Retired Gen. Louis C. Wagner",
                "rank": "U.S. Army, AUSA Senior Fellow",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Heidi Shyu",
                "rank": "Assistant Secretary of the Army(Acquisition, Logistics and Technology and Acquisition Executive)",
                "type": "lead speaker"
            }
        ],
        "panelists": [
            {
                "name": "Mary Miller",
                "rank": "DASA-R&T",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen. James O. Barclay III",
                "rank": "Deputy Chief of Staff, U.S. Army G-8",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen. Patricia E. McQuistion",
                "rank": "Deputy Commanding General/Chief of Staff, U.S. Army Materiel Command",
                "type": "panelists"
            },
            {
                "name": "Helen Greiner",
                "rank": "CEO, CyPhy Works (S&T)",
                "type": "panelists"
            },
            {
                "name": "Prof. John D. Joannopoulos",
                "rank": "Massachusetts Institution of Technology, Institute for Soldier Nanotechnologies",
                "type": "panelists"
            },
            {
                "name": "Dr. Randy Hill, Ph.D",
                "rank": "University of Southern California, Institute for Creative Technologies",
                "type": "panelists"
            },
            {
                "name": "Russ Young",
                "rank": "FedEx Express Global Operations",
                "type": "panelists"
            }
        ],
        "start": "1413379800000",
        "subtitle": "Delivering Innovation for the Army",
        "title": "ILW Contemporary Military Forum VII",
        "twitter": "CMF",
        "video_id": "4975"
    },
    {
        "display_time": "10 a.m. - noon",
        "end": "1413388800000",
        "introductions_by": [
            {
                "name": "Retired Command Sgt. Maj. Daniel K. Elder",
                "rank": "U.S. Army, AUSA Senior Fellow",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Lt. Gen.  Robert B. Brown",
                "rank": "Commanding General, U.S. Army Combined Arms Center",
                "type": "lead speaker"
            }
        ],
        "panelists": [
            {
                "name": "Lt. Gen. Patricia D. Horoho",
                "rank": "Commanding General, U.S. Army Medical Command/The Surgeon General, U.S. Army",
                "type": "panelists"
            },
            {
                "name": "Maj. Gen. Eric P. Wendt",
                "rank": "Commanding General, John F. Kennedy Special Warfare Center and School",
                "type": "panelists"
            },
            {
                "name": "Brad R. Carson",
                "rank": "Under Secretary of the U.S. Army",
                "type": "panelists"
            },
            {
                "name": "Dr. Michelle Sams, Ph.D.",
                "rank": "Director, U.S. Army Research Institute for the Behavioral and Social Sciences",
                "type": "panelists"
            },
            {
                "name": "Ori Brafman",
                "rank": "Coauthor, \"The Starfish and the Spider and Sway\"",
                "type": "panelists"
            }
        ],
        "start": "1413381600000",
        "subtitle": "The Human Dimension",
        "title": "ILW Contemporary Military Forum VIII",
        "twitter": "CMF",
        "video_id": "4978"
    },
    {
        "display_time": "2-4 p.m.",
        "end": "1413403200000",
        "introductions_by": [
            {
                "name": "Retired Gen. William \"Buck\" Kernan",
                "rank": "U.S. Army, AUSA Senior Fellow",
                "type": "introductions by"
            }
        ],
        "lead_speaker": [
            {
                "name": "Lt. Gen. Patrick J. Donahue II",
                "rank": "Deputy Commanding General, U.S. Army Forces Command",
                "type": "lead speaker"
            }
        ],
        "panelists": [
            {
                "name": "Gen. Vincent K. Brooks",
                "rank": "Commanding General, U.S. Army Pacific",
                "type": "panelists"
            },
            {
                "name": "Lt. Gen.  Jeffrey W. Talley",
                "rank": "Chief of Army Reserve/Commanding General, U.S. Army Reserve Command",
                "type": "panelists"
            },
            {
                "name": "Michael E. O\u2019Hanlon",
                "rank": "Senior Fellow, Center for 21st Century Security and Intelligence/Director, Research for the Foreign Policy Program, Brookings Institution",
                "type": "panelists"
            },
            {
                "name": "Andrew Krepinevich",
                "rank": "President, Center for Strategic and Budgetary Assessments",
                "type": "panelists"
            }
        ],
        "start": "1413396000000",
        "subtitle": "Regionally Aligned Forces: A Globally Responsive and Regionally Engaged Army",
        "title": "ILW Contemporary Military Forum IX",
        "twitter": "CMF",
        "video_id": "4981"
    },
    {
        "display_time": "2-4 p.m.",
        "end": "1413403200000",
        "lead_speaker": [
            {
                "name": "Lt. Gen. Edward C. Cardon",
                "rank": "Commanding General, U.S. Army Cyber Command",
                "type": "lead speaker"
            }
        ],
        "moderator": [
            {
                "name": "Kevin M. Fahey",
                "rank": "Program Executive Officer, Combat Support and Combat Service Support",
                "type": "moderator"
            }
        ],
        "part_i_panelists": [
            {
                "name": "Maj. Gen. Stephen G. Fogarty",
                "rank": "Commanding General, Cyber Center of Excellence",
                "type": "part i panelists"
            },
            {
                "name": "Maj. Gen. John B. Morrison, Jr.",
                "rank": "Deputy Commanding General, 2nd Army/Commanding General, NETCOM",
                "type": "part i panelists"
            },
            {
                "name": "Col.  Mark C. DiTrolio",
                "rank": "Commander Army Reserve, Cyber Operations Group",
                "type": "part i panelists"
            },
            {
                "name": "Col. William J. Hartman",
                "rank": "Commander, 780th Military Intelligence Brigade",
                "type": "part i panelists"
            }
        ],
        "part_ii_panelists": [
            {
                "name": "Bruce Potter",
                "rank": "CTO Pointe Technologies",
                "type": "part ii panelists"
            },
            {
                "name": "John Serafini",
                "rank": "Vice President, Allied Minds",
                "type": "part ii panelists"
            },
            {
                "name": "Frank Heidt",
                "rank": "CEO, Leviathan Security Group",
                "type": "part ii panelists"
            }
        ],
        "start": "1413396000000",
        "subtitle": "Increasing Cyber Capabilities for the Army",
        "title": "ILW Contemporary Military Forum X",
        "twitter": "CMF",
        "video_id": "4983"
    }
];

var map = {
	"speakers" : "Speakers",
	"keynote_speaker" : "Keynote Speaker",
	"introductions_by" : "Introductions By",
	"lead_speaker"  : "Lead Speaker",
	"opening_remarks" : "Opening Remarks",
	"opening_speaker" : "Opening Speaker",
	"special_address_by" : "Special Address By",
	"moderator" : "Moderator",
	"part_i_moderator" : "Part I Moderator",
	"part_ii_moderator" : "Part II Moderator",
	"panelists" : "Panelists",
	"part_i_panelists" : "Part I Panelists",
	"part_ii_panelists" : "Part II Panelists",
	"closing_speaker" : "Closing Speaker",
	"closing_remarks" : "Closing Remarks"
};
	
function GetURLParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

var ilw = {} || ilw;

ilw = {
	current_url: "",

	player: function(stream_id) {
		var feed_width = parseInt($("#player").width()),
			player_w = "",
			player_h = "";

		player_w = feed_width;
		if (player_w > 720) {
			player_w = 720;
		}
		player_h = Math.floor(player_w * 9 / 16);

		$(".ausa_left #player").append(
			'<iframe width="' + player_w + '" height="' + player_h + '" scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: ' + player_w + 'px; height: ' + player_h + 'px;" allowtransparency="true" src="http://www.dvidshub.net/webcast/embed/' + stream_id + '?show_description=0&width=' + player_w + '&height=' + player_h + '"></iframe>'
		);
	},

	add_title: function(item) {
		return (item.title) ? item.title : "";
	},

	get_video_info: function(stream_id) {
		// loop through items and find the current video's title and desc
		var title = $("<h3>"),
			p = $("<p>");

		for (var i = 0; i < schedule_obj.length; i++) {
			if (schedule_obj[i].video_id == stream_id) {
				title.append(
					ilw.add_title(schedule_obj[i])
				);
				p.append(
					ilw.add_description(schedule_obj[i])
				);
			}
		}

		$(".video_description").append(title).append(p);
	},

	facebook: function() {
		var div = '<div class="fb-comments" data-href="' + ilw.current_url + window.location.search + '" data-width="440" data-numposts="3" data-colorscheme="light"></div>';

		$(".fb_feed").html(div);
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	},

	twitter: function(twitter_feed) {
		var hashtag,
			twitter_url,
			widget_ids = {
				"2014": "516603003181076482",
				"FAM2014": "519853032847065089",
				"CMF": "516604505748549632",
				"TEST2014": "519551908847951873"
			},
			widget_id = widget_ids[twitter_feed];

		hashtag = "AUSA" + twitter_feed.substr(0, 1).toUpperCase() + twitter_feed.substr(1);
		twitter_url = "https://twitter.com/hashtag/" + hashtag;

		$("#twitter_feed_name").html(hashtag);

		if (!$(".twitter_feed").html()) {
			$(".twitter_feed").append(
				'<a class="twitter-timeline" width="250" height="350" href="' + twitter_url + '" data-widget-id="' + widget_id + '">#' + hashtag + '</a>',
				'<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>'
			);
		}
	},

	add_description: function(item) {
		var desc = $("<div>"),
			text = $("<div>").prop({
				"class": "indent"
			});
		if (item.description) {
			text.html(item.description);
		} 

		if (!ilw.current_url.match("live/") && item.description) {
			desc.html("Description:");
		}

		desc.append(text);
		return desc;
	},
	
	add_speakers: function(item) {
		var speakers = $("<div>");
		for(var key in item) {
			var list = $("<ul>");
			if(item.hasOwnProperty(key)){
				if (key in map) {
					var objects = item[key];
					speakers.append(map[key] + ":<br />");
					for (var i = 0; i < objects.length; i++) {
						var li = $("<li>");
						if (objects[i].rank) {
							li.html(objects[i].name + ", " + objects[i].rank);
						} else {
							li.html(objects[i].name);
						}
						list.append(li);
					}
					speakers.append(list);
				}
			}
		}
		
		return speakers;
	},
	
	add_url: function(item, day) {
		var link = $("<a>"),
			curr_time = parseInt(GetURLParameter("time")) || new Date().getTime();

		// remove the live/ part if we are already on a live page, since we add it by default below

		link.prop({
			"href": ilw.current_url.replace("live/", "") + "live/?id=" + item.video_id + "&day=" + day
		}).text(item.title);

		if (curr_time > item.start && curr_time < item.end) {
			link.append("<br />LIVE: View Web Stream").css("color", "red");
		}

		return link;
	},

	get_current_url: function() {
		if (!window.location.origin) {
			window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
		}

		ilw.current_url = window.location.origin + window.location.pathname;
	},

	drop_down: "closed",

	open_close_dd: function() {
		if (ilw.drop_down == "closed") {
			$('#group_select_dl dd').show();
			ilw.drop_down = "open";
		} else {
			$('#group_select_dl dd').hide();
			ilw.drop_down = "closed";
		}
	},

	schedule: function(items) {
		var days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'],
			landing = $("body").prop("id") == "index",
			stream_id = GetURLParameter("id");

		// loop through schedule obj, add items to page
		for (var i = 0; i < items.length; i++) {
			var div = $("<div>").prop({
				"class": "item_wrap"
			});

			var event_date = new Date(parseInt(items[i].start));
			var d = days[event_date.getDay()];

			if (stream_id == items[i].video_id) {
				div.addClass("active");
			}
			
			var h4 = $("<h4>");
			
			div.append(
				$("<h2>").append(ilw.add_url(items[i], d)),
				h4.html(items[i].display_time + " EDT")
			);
			
			if(items[i].subtitle){
				h4.before($("<h4>").append(items[i].subtitle).css("font-style","italic"));
			}

			if (landing) {
				div.append(ilw.add_description(items[i]));
			}

			div.append(
				ilw.add_speakers(items[i])
			);

			if ($('#search_box').val() === "") {
				$("#schedule_items ." + d).append(div);
			} else {
				$("#schedule_items .search_results").show().append(div);
			}
		}
	},

	perform_search: function(token, keywords) {
		var patt;
		try {
			patt = new RegExp(token, "gi");
			return (patt.exec(keywords)) ? 1 : 0;
		} catch (e) {
			return 0;
		}
	},

	filter_items: function(group) {
		var groups = {
			"0": "Mon",
			"1": "Tues",
			"2": "Wed"
		};

		$(".search_results").html("");
		$(".Mon, .Tues, .Wed, .search_results").hide();
		$("#schedule_items ." + groups[group]).show();
	},

	events: function() {

		$('#group_select_dl, #schedule_groups #arrow').click(function(e) {
			e.stopPropagation();
			ilw.open_close_dd();
		});

		$("html").click(function(e) {
			e.stopPropagation();
			if (ilw.drop_down == "open") {
				ilw.open_close_dd();
			}
		});

		$('#group_select_dl dd').click(function(e) {
			e.stopPropagation();
			var group = $(this).data('value');
			var group_txt = $(this).text();
			$('#group_select_dt').data('selected-val', group).text(group_txt);
			$('#search_box').val(''); // clear search value
			$('#group_select_dl dd').hide(); // hide "drop down"
			ilw.drop_down = "closed";
			
			ilw.filter_items(group);
		});
		
		$("#schedule_search").on('click','.reset_icon',function(){
			var hasClass = $(this).hasClass("reset_icon");
			if(hasClass){
				$(this).removeClass("reset_icon");
				$('#search_box').val("");
				$(".search_results").html("").hide();
				$(".Mon").show();
			}
		});
		
		$('#search_box').keyup(function() {
			var matches = [],
				search_term = $(this).val();
				
			if (search_term) {
				$(".search_icon").addClass("reset_icon");
				for (var i = 0; i < schedule_obj.length; i++) {
					var hits = 0;
					hits += ilw.perform_search(search_term, schedule_obj[i].title);
					if (schedule_obj[i].description) {
						hits += ilw.perform_search(search_term, schedule_obj[i].description);
					}
					if (schedule_obj[i].speakers) {
						for (var j = 0; j < schedule_obj[i].speakers.length; j++) {
							hits += ilw.perform_search(search_term, schedule_obj[i].speakers[j].name);
							hits += ilw.perform_search(search_term, schedule_obj[i].speakers[j].rank);
						}
					}

					if (hits) {
						matches.push(schedule_obj[i]);
					}
				}
				$(".Mon, .Tues, .Wed").hide();
				if (matches.length) {
					$(".search_results").html("");
					ilw.schedule(matches);
				} else {
					$('.search_results').show().html("<div class='item_wrap'><h2>No results found!</h2></div>");
				}
			} else {
				$(".search_icon").removeClass("reset_icon");
				$(".search_results").html("").hide();
				$(".Mon").show();
			}
		});
		

		// add click event to entire div
		$(".item_wrap").on("click", function() {
			var link = $(this).find("a").prop("href");
			window.location = link;
			return false;
		});
	},

	show_selected_day: function(index) {
		var group_txt = $("#group_select_dl dd:eq(" + index + ")").html();
		$('#group_select_dt').data('selected-val', index).text(group_txt);
		$('#search_box').val(''); // clear search value
		$('#group_select_dl dd').hide(); // hide "drop down"
		ilw.drop_down = "closed";

		ilw.filter_items(index);
	},
	
	show_current_day: function(time) {
		var text, times =
				[{
				"start": "1413172800000",
				"end": "1413259199000"
			}, {
				"start": "1413259200000",
				"end": "1413345599000"
			}, {
				"start": "1413345600000",
				"end": "1413431999000"
			}];
		for (var i = 0; i < times.length; i++) {
			if (time > times[i].start && time < times[i].end) {
				text = $("#group_select_dl dd").eq(i).text();
				$("#group_select_dt").text(text);
				ilw.filter_items(i);
			}
		}
	}
};

$(document).ready(function() {
	var curr_time = parseInt(GetURLParameter("time")) || new Date().getTime(),
		url_id, url_day, stream_id, twitter_feed, live_list = [];

	url_id = GetURLParameter("id"); // get the video id from the url
	url_day = GetURLParameter("day");

	if (url_day == "Wed") {
		ilw.show_selected_day(2);
	} else if (url_day == "Tues") {
		ilw.show_selected_day(1);
	} else if (url_day == "Mon") {
		ilw.show_selected_day(0);
	} else {
		ilw.show_current_day(curr_time);
	}

	// initial loop through obj to get the right stream_id and twitter feed
	for (var i = 0; i < schedule_obj.length; i++) {

		if (curr_time > schedule_obj[i].start && curr_time < schedule_obj[i].end) {
			live_list.push(schedule_obj[i]);
		}

		if (schedule_obj[i].video_id == url_id) {
			twitter_feed = schedule_obj[i].twitter;
		}
	}

	if (typeof url_id !== "undefined") {
		stream_id = url_id;
	} else {
		if (live_list.length) {
			stream_id = live_list[0].video_id;
			twitter_feed = live_list[0].twitter;
		} else {
			stream_id = schedule_obj[0].video_id;
			twitter_feed = "2014";
		}
	}

	ilw.get_current_url();
	ilw.player(stream_id);
	ilw.twitter(twitter_feed);

	if ($("body").prop("id") != "index") {
		ilw.get_video_info(stream_id);
		ilw.facebook();
	}

	ilw.schedule(schedule_obj);

	ilw.events();

});
